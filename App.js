import React from 'react';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import {persistStore, persistReducer} from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import {PersistGate} from 'redux-persist/integration/react';
import reducer from "./src/state";
import EntryPoint from "./src/navigation";

const persistConfig = {
  key: 'root',
  storage
};

const persistedReducer = persistReducer(persistConfig, reducer);
const store = createStore(persistedReducer);
// const store = createStore(reducer);
const persistor = persistStore(store);

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <EntryPoint />
        </PersistGate>
      </Provider>
    )
  }
}